import React, { Component } from 'react';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import Graph from './Graph.js';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      total_budget: 770,
      emissions_2018: 40,
      wait_years: 2,
      yearly_cut: 1.7
    }
  }

  valueChanged = (val) => {
    this.setState({ yearly_cut: val });
  }

  waitValueChanged = (val) => {
    this.setState({ wait_years: val });
  }

  buildData = () => {
    let years = Array.from(
        new Array(2100-2019),
        (val,index) => 2019 + index);

    let data = [];
    let remaining_budget = this.state.total_budget;
    let emission_prev = this.state.emissions_2018;
    let wait_years = this.state.wait_years;
    const cut = 1 - (this.state.yearly_cut / 100.0);

    for(let year in years) {
      if (wait_years > 0) {
        wait_years = wait_years - 1;
      } else {
        emission_prev = emission_prev * cut;
      }
      remaining_budget = remaining_budget - emission_prev;
      data.push(remaining_budget);
    };

    return({
      labels: years,
      series: [ data ]
    });
  }

  zeroYear = () => {
    let budget = this.state.total_budget;
    let a = this.state.emissions_2018;
    let r = 1 - (this.state.yearly_cut / 100.0);
    budget = budget - (a * this.state.wait_years);
    let sum = a / (1-r);
    if (sum < budget) {
      return false;
    } else {
      let remain = this.state.total_budget;
      budget = budget - (a * this.state.wait_years);
      let emiss = a;
      let year = 2018;
      let wait_years = this.state.wait_years;
      while(year < 2200 && remain > 0) {
        if (wait_years > 0) {
          wait_years = wait_years - 1;
        } else {
          emiss = emiss * r;
        }
        remain = remain - emiss;
        year = year + 1;
      }
      return(year);
    }
  }

  render() {
    let slidermarks = {
      5.0: "5 %",
      10.0: "10 %"
    }
    let year = this.zeroYear();
    const text = year ? "Du må kutte mer! Med denne kutt-takten har du brukt opp verdens karbonbudsjett i år " + year + "." : "Gratulerer. Nå er karbonbudsjettet i balanse."

    const cssClass = year ? 'unbalanced' : 'balanced';

    return(
        <div className="kalkisen">
          <h2>Hvor mye må vi kutte?</h2>
          <p>Målet er å holde den globale oppvarmingen under 1,5°. Ifølge FNs klimapanel må vi holde de samlede utslippene i all fremtid under 770 Gtonn med CO<sub>2</sub>, dersom vi skal ha mer enn femti prosent sjanse for å lykkes med dette målet.</p>
 
          <p>Takten i den nødvendige kuttingen avhenger av når vi begynner. Prøv selv: Hvor raskt må vi kutte dersom vi venter fem år med å begynne? Ti år?</p>

          <p>År med venting før kuttingen begynner:</p>
          <div className="slideandnumber">
          <Slider
            defaultValue={this.state.wait_years}
            onChange={this.waitValueChanged}
            step={1}
            min={1}
            max={10}
            marks={ {1: 1, 5: 5, 10: 10} }
          />
          <div className="number">
            { this.state.wait_years } år
          </div>
          </div>

          <p>Deretter kutter verden denne prosentandelen av de totale CO<sub>2</sub>-utslippene hvert år:</p>
          <div className="slideandnumber">
          <Slider
            defaultValue={this.state.yearly_cut}
            onChange={this.valueChanged}
            step={0.1}
            min={0.1}
            max={15.0}
            marks={slidermarks}
          />
          <div className="number">
            { this.state.yearly_cut } %
          </div>
          </div>

          <p>Få streken over null!</p>
          <p>Figuren viser gjenværende saldo på karbonbudsjettet med { this.state.yearly_cut } % årlig kutt fra { this.state.wait_years + 2018 }.</p>
          <Graph data={ this.buildData() } cssClass={ cssClass } />

          <div className={ "budget " + cssClass }>
            { text  }
          </div>

          <p><small>Beregningene er Morgenbladets egne, basert på tall fra <a href="http://www.ipcc.ch/report/sr15/">Intergovernmental Panel on Climate Change (2018): Global Warming of 1.5 °C</a>.</small></p>
        </div>
      );
  }
}

export default App;
