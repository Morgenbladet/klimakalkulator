import React, { Component } from 'react';
import ChartistGraph from 'react-chartist';

class Graph extends Component {

  interpolateX(value,index) {
    return value % 20 === 0 ? value : null;
  }

  render() {
    let data = this.props.data;
    let options = {
      low: 0,
      high: 800,
      showPoint: false,
      showArea: true,
      axisX: {
        labelInterpolationFnc: this.interpolateX
      }
    };
    let type = 'Line';
    let className="ct-octave " + this.props.cssClass;

    return(<ChartistGraph
      className= { className }
      data={data}
      options={options}
      type={type}
      />);
  }
}

export default Graph;
